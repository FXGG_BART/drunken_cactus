﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "DrunkenCactus/PowerUps")]
public class PowerUpData : ScriptableObject
{
    [System.Serializable]
    public struct PowerUp
    {
        public string _name;
        public float _timer;
        public float _value;
    }

    public List<PowerUp> powerUps = new List<PowerUp>();
}