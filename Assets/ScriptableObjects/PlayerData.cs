﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "DrunkenCactus/Player")]
public class PlayerData : ScriptableObject
{
    [System.Serializable]
    public struct Player
    {
         public string _name;       
    }

    public List<Player> players = new List<Player>();
}
