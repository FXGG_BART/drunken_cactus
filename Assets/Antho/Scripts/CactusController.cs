﻿using System.Collections;
using UnityEngine;

public class CactusController : MonoBehaviour
{
    [SerializeField] private float _playerAccel = 1.0f;
    [SerializeField] private float _playerMaxSpeed = 3.0f;
    [SerializeField] private float _playerThrowPower = 1.0f;
    [SerializeField] private float _maxHP = 20f;
    [SerializeField] private PickZone _pickZone = null;
    [SerializeField] private Transform _boxHeld = null;
    [SerializeField] private PowerUpData _data;
    [SerializeField] private Animator _animator;


    private GameObject _heldCrate = null;
    private CrateBehaviour _heldCrateBehaviour = null;
    private Rigidbody _rigid;

    private Vector3 _forwardMemory;

    private bool _spacebar;
    private float _currentHP;
    private int _invertion = 1;
    private bool _isInvincible = false;
    private bool _isSlowed = false;

    private int _enterNmb = 0;


    private void Start()
    {
        _rigid = gameObject.GetComponent<Rigidbody>();

        _currentHP = _maxHP;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            _enterNmb++;

            if (_enterNmb >= 3)
            {
                TimeManager.Instance.TimerStart();
            }
        }

        if(TimeManager.Instance.CanPlayersMove() == true)
        {
            Controls();
        }

    }


    private void Controls()
    {
        if (_rigid.velocity.magnitude > 0.01f)
        {
            transform.forward = _rigid.velocity.normalized;
        }
        else
        {
            transform.forward = _forwardMemory;
        }
        if (_rigid.velocity.magnitude < 0.001f)
        {
            _rigid.velocity = Vector3.zero;
            _rigid.angularVelocity = Vector3.zero;
        }

        if (_rigid.velocity.magnitude < _playerMaxSpeed)
        {
            if (Input.GetKey(KeyCode.W))
            {
                _rigid.AddForce(Vector3.forward * _playerAccel * Time.deltaTime * _invertion, ForceMode.Acceleration);
                _animator.SetBool("Walk", true);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                _rigid.AddForce(Vector3.back * _playerAccel * Time.deltaTime * _invertion, ForceMode.Acceleration);
                _animator.SetBool("Walk", true);
            }
            else if (Input.GetKey(KeyCode.A))
            {
                _rigid.AddForce(Vector3.left * _playerAccel * Time.deltaTime * _invertion, ForceMode.Acceleration);
                _animator.SetBool("Walk", true);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                _rigid.AddForce(Vector3.right * _playerAccel * Time.deltaTime * _invertion, ForceMode.Acceleration);
                _animator.SetBool("Walk", true);
            }
            else
            {
                _animator.SetBool("Walk", false);
            }

        }
        else
        {
            _rigid.velocity = _rigid.velocity.normalized * (float)(_playerMaxSpeed - 0.05f);


        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            _spacebar = true;
        }
        _forwardMemory = transform.forward;
    }

    private void FixedUpdate()
    {
        if (_spacebar)
        {
            if (_heldCrate == null)
            {
                GameObject crate = _pickZone.PickUpClosest();
                Debug.Log(crate);
                if (crate != null)
                {
                    _heldCrate = crate;
                    CrateBehaviour cratebehave = crate.GetComponent<CrateBehaviour>();
                    _heldCrateBehaviour = cratebehave;
                    cratebehave.CratePickUp(_boxHeld);
                }
            }
            else
            {
                _heldCrateBehaviour.CrateThrown(_playerThrowPower);
                _heldCrateBehaviour = null;
                _heldCrate = null;
            }
        }
        _spacebar = false;
    }

    public void TakeDamage(int damage)
    {
        if (!_isInvincible)
        {
            _currentHP -= damage;
            _animator.SetTrigger("Hit");
        }

        if (_currentHP <= 0)
        {
            Debug.Log("player is dead");
        }
    }

    public void PowerupPickup(int type)
    {
        switch (type)
        {
            case (0):
                _isInvincible = true;
                StartCoroutine(InvincibilityTimer());
                break;

            case (1):
                _isSlowed = true;
                _playerMaxSpeed = _playerMaxSpeed * _data.powerUps[1]._value;
                StartCoroutine(SlowTimer());
                break;

            case (2):
                _invertion = -1;
                StartCoroutine(InvertionTimer());
                break;

            case (3):
                _currentHP += _data.powerUps[3]._value;
                if (_currentHP > _maxHP)
                {
                    _currentHP = _maxHP;
                }
                break;

            case (4):
                break;

        }
    }



    //*************** Coroutines ***************

    private IEnumerator InvincibilityTimer()
    {
        float timer = 0f;
        while (timer < _data.powerUps[0]._timer)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        _isInvincible = false;
    }

    private IEnumerator SlowTimer()
    {
        float timer = 0f;
        while (timer < _data.powerUps[1]._timer)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        _isSlowed = false;
        _playerMaxSpeed = 3.0f;
    }

    private IEnumerator InvertionTimer()
    {
        float timer = 0f;
        while (timer < _data.powerUps[2]._timer)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        _invertion = 1;
    }

}