﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DrunkenCactus
{
    public class UI_Buttons : MonoBehaviour
    {
        [SerializeField] AudioClip _hoverClip;

        private Animator _buttonAnimator;
        private bool _isHover = false;
        private AudioSource _hoverSound;
        

        private void Start()
        {
            _buttonAnimator = GetComponent<Animator>();
            _hoverSound = GetComponent<AudioSource>();
        }
        public void IsHoverCheck()
        {
            _isHover = true;
        }

        private void Update()
        {
            OnHover();
        }

        private void OnHover()
        {
            if(_isHover)
            {
                _buttonAnimator.SetBool("OnHover", true);
            }
            else
            {
                _buttonAnimator.SetBool("OnHover", false);
            }
        }

        public void IsHoverExit()
        {
            _isHover = false;
        }

        public void OnHoverClick()
        {
            _buttonAnimator.SetTrigger("Click");
            _hoverSound.PlayOneShot(_hoverClip);
        }

        public void PlayGame()
        {
            SceneManager.LoadScene("Hello");
        }
    }
}

