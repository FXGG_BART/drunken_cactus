﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DrunkenCactus
{
    public class Conditions : MonoBehaviour
    {
        [SerializeField] private GameObject _blurBG;
        [SerializeField] private GameObject _inGame;
        [SerializeField] private GameObject _objectivesConditions;
        [SerializeField] private GameObject _inputsConditions;
        [SerializeField] private GameObject _specialItemsConditions;

        private int conditionIndex = 1;

        private void Start()
        {
            _inGame.SetActive(false);
            _inputsConditions.SetActive(false);
            _specialItemsConditions.SetActive(false);
            _blurBG.SetActive(true);
        }

        private void Update()
        {
            NextConditions();
            SwitchConditions();
        }

        private void NextConditions()
        {
            if(Input.GetKeyDown(KeyCode.Return))
            {
                conditionIndex += 1;
            }
        }

        private void SwitchConditions()
        {
            switch(conditionIndex)
            {
                case 2:
                    _objectivesConditions.SetActive(false);
                    _inputsConditions.SetActive(true);
                    _specialItemsConditions.SetActive(false);
                    break;
                case 3:
                    _objectivesConditions.SetActive(false);
                    _inputsConditions.SetActive(false);
                    _specialItemsConditions.SetActive(true);
                    break;
                case 4:
                    _objectivesConditions.SetActive(false);
                    _inputsConditions.SetActive(false);
                    _specialItemsConditions.SetActive(false);
                    _blurBG.SetActive(false);
                    _inGame.SetActive(true);
                    break;

            }
        }

    }

}
