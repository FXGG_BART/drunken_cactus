﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float _playerAccel = 1.0f;
    [SerializeField] private float _playerMaxSpeed = 3.0f;
    [SerializeField] private float _playerThrowPower = 1.0f;
    [SerializeField] private float _playerInvulPeriod = 0.3f;
    [SerializeField] private float _maxHP = 20f;
    [SerializeField] private PickZone _pickZone = null;
    [SerializeField] private Transform _boxHeld = null;
    [SerializeField] private PowerUpData _data;
    [SerializeField] private List<AudioClip> _throwClips = new List<AudioClip>();
    [SerializeField] private List<AudioClip> _pickUpClips = new List<AudioClip>();
    [SerializeField] private int _playerIndex;
    [SerializeField] private Animator _animator;
    [SerializeField] private Slider _hpBar;

    private GameObject _heldCrate = null;
    private CrateBehaviour _heldCrateBehaviour = null;
    private Rigidbody _rigid;
    private AudioSource _audio;

    private Vector3 _forwardMemory;

    private bool _spacebar;
    private float _currentHP;
    private int _invertion = 1;
    private bool _isInvincible = false;
    private bool _isSlowed = false;
    private int _enterNmb = 0;

    private void Start()
    {
        _rigid = gameObject.GetComponent<Rigidbody>();
        _audio = gameObject.GetComponent<AudioSource>();

        _currentHP = _maxHP;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) && !TimeManager.Instance.CanPlayersMove() && _playerIndex == 0)
        {
            if(_enterNmb < 3)
            {
                _enterNmb++;
            }

            if (_enterNmb >= 3)
            {
                TimeManager.Instance.TimerStart();
                _enterNmb = 0;
            }

        }
        if (_rigid.velocity.magnitude > 0.01f)
        {
            transform.forward = _rigid.velocity.normalized;
            //_animator.SetBool("Walk", true);
        }
        else
        {
            transform.forward = _forwardMemory;
            //_animator.SetBool("Walk", false);
        }
        if (_rigid.velocity.magnitude < 0.001f)
        {
            _rigid.velocity = Vector3.zero;
            _rigid.angularVelocity = Vector3.zero;
        }

        if (_rigid.velocity.magnitude < _playerMaxSpeed && TimeManager.Instance.CanPlayersMove())
        {
            if (_playerIndex == 0)
            {
                if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
                {
                    _animator.SetBool("Walk", true);
                }
                else
                {
                    _animator.SetBool("Walk", false);
                }

                if (Input.GetKey(KeyCode.W))
                {
                    _rigid.AddForce(Vector3.forward * _playerAccel * Time.deltaTime * _invertion, ForceMode.Acceleration);

                }
                else if (Input.GetKey(KeyCode.S))
                {
                    _rigid.AddForce(Vector3.back * _playerAccel * Time.deltaTime * _invertion, ForceMode.Acceleration);
                }


                if (Input.GetKey(KeyCode.A))
                {
                    _rigid.AddForce(Vector3.left * _playerAccel * Time.deltaTime * _invertion, ForceMode.Acceleration);
                }
                else if (Input.GetKey(KeyCode.D))
                {
                    _rigid.AddForce(Vector3.right * _playerAccel * Time.deltaTime * _invertion, ForceMode.Acceleration);
                }
            }
            if (_playerIndex == 1)
            {
                if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
                {
                    _animator.SetBool("Walk", true);
                }
                else
                {
                    _animator.SetBool("Walk", false);
                }

                if (Input.GetKey(KeyCode.UpArrow))
                {
                    _rigid.AddForce(Vector3.forward * _playerAccel * Time.deltaTime * _invertion, ForceMode.Acceleration);

                }
                else if (Input.GetKey(KeyCode.DownArrow))
                {
                    _rigid.AddForce(Vector3.back * _playerAccel * Time.deltaTime * _invertion, ForceMode.Acceleration);
                }


                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    _rigid.AddForce(Vector3.left * _playerAccel * Time.deltaTime * _invertion, ForceMode.Acceleration);
                }
                else if (Input.GetKey(KeyCode.RightArrow))
                {
                    _rigid.AddForce(Vector3.right * _playerAccel * Time.deltaTime * _invertion, ForceMode.Acceleration);
                }
            }
        }
        else
        {
            
            _rigid.velocity = _rigid.velocity.normalized * (float)(_playerMaxSpeed - 0.05f);
        }
        if (_playerIndex == 0)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _spacebar = true;
            }
        }
        if (_playerIndex == 1)
        {
            if (Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                _spacebar = true;
            }
        }
        _forwardMemory = transform.forward;
    }

    private void FixedUpdate()
    {
        
        if (_spacebar)
        {
            if (_heldCrate == null)
            {
                GameObject crate = _pickZone.PickUpClosest();
                Debug.Log(crate);
                if (crate != null)
                {
                    _audio.PlayOneShot(_pickUpClips[Random.Range(0, 3)]);
                    _heldCrate = crate;
                    CrateBehaviour cratebehave = crate.GetComponent<CrateBehaviour>();
                    _heldCrateBehaviour = cratebehave;
                    cratebehave.CratePickUp(_boxHeld);
                }
            }
            else
            {
                _heldCrateBehaviour.CrateThrown(_playerThrowPower);
                _audio.PlayOneShot(_throwClips[Random.Range(0, 3)]);
                _heldCrateBehaviour = null;
                _heldCrate = null;
            }
        }
        _spacebar = false;
    }

    public void TakeDamage(int damage)
    {
        if (!_isInvincible)
        {
            _currentHP -= damage;
            _hpBar.value = _currentHP / _maxHP;
            Debug.Log($"{_playerIndex} : {damage}");
        }

        if (_currentHP <= 0)
        {
            TimeManager.Instance.PlayerDown(_playerIndex);
        }

        StartCoroutine(InvincibilityTimer(_playerInvulPeriod));
    }

    public void PowerupPickup(int type)
    {
        switch (type)
        {
            case (0):
                _isInvincible = true;
                StartCoroutine(InvincibilityTimer());
                break;

            case (1):
                _isSlowed = true;
                _playerMaxSpeed = _playerMaxSpeed * _data.powerUps[1]._value;
                StartCoroutine(SlowTimer());
                break;

            case (2):
                _invertion = -1;
                StartCoroutine(InvertionTimer());
                break;

            case (3):
                _currentHP += _data.powerUps[3]._value;
                if (_currentHP > _maxHP)
                {
                    _currentHP = _maxHP;
                }
                break;

            case (4):
                break;
       
        }
    }



 //*************** Coroutines ***************

    private IEnumerator InvincibilityTimer()
    {
        float timer = 0f;
        while(timer < _data.powerUps[0]._timer)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        _isInvincible = false;
    }

    private IEnumerator InvincibilityTimer(float inviz)
    {
        float timer = 0f;
        while (timer < inviz)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        _isInvincible = false;
    }



    private IEnumerator SlowTimer()
    {
        float timer = 0f;
        while (timer < _data.powerUps[1]._timer)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        _isSlowed = false;
        _playerMaxSpeed = 3.0f;
    }

    private IEnumerator InvertionTimer()
    {
        float timer = 0f;
        while (timer < _data.powerUps[2]._timer)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        _invertion = 1;
    }

}