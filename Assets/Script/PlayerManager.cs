﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public event Action<PlayerController> OnPlayerAssigned = default;
    public static PlayerManager Instance;
    private PlayerController playerController;

    public PlayerController PlayerController => playerController;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SetPlayerController(PlayerController playerController)
    {
        this.playerController = playerController;
        OnPlayerAssigned?.Invoke(playerController);
    }

}
