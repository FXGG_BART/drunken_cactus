﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "DrunkenCactus/Crate")]
public class CrateData : ScriptableObject
{
    [System.Serializable]
    public struct Crate
    {
        public string _type;
        public Mesh _mesh;
        public Material _material;
        public int _damage;
        public float _timer;
        public bool _explosive;
        public float _explosionRadius;
    }

    public List<Crate> crates = new List<Crate>();
}
