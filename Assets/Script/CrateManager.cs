﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrateManager : MonoBehaviour
{
    private static CrateManager _crateManager;

    [SerializeField] private GameObject _cratePrefab = null;
    [SerializeField] private int _numberOfTotalCrate = 12;

    private List<GameObject> _inactiveCrate;
    private List<GameObject> _activeCrate;

    private float _timer;


    public static CrateManager Instance
    {
        get
        {
            if(_crateManager == null)
            {
                GameObject go = new GameObject("CrateManager");
                go.AddComponent<CrateManager>();
            }
            return _crateManager;
        }
    }

    private void Awake()
    {
        _crateManager = this;
        
        _inactiveCrate = new List<GameObject>();
        _activeCrate = new List<GameObject>();

        for (int i = 0; i < _numberOfTotalCrate; i++)
        {
            GameObject crate = Instantiate(_cratePrefab, transform);
            crate.SetActive(false);
            _inactiveCrate.Add(crate);
        }

        SpawnCrate();
        SpawnCrate();
        SpawnCrate();
    }

    private void SpawnCrate()
    {
        if (_inactiveCrate.Count != 0)
        {
            
            GameObject go = _inactiveCrate[0];
            _inactiveCrate.RemoveAt(0);
            Vector3 spawnloc = new Vector3(Random.Range(-4.5f, 4.5f), 3.0f, Random.Range(-4.5f, 4.5f));
            
            go.SetActive(true);
            go.transform.position = spawnloc;
            Rigidbody rbgo = go.GetComponent<Rigidbody>();
            rbgo.velocity = rbgo.velocity.normalized;
            _activeCrate.Add(go);
        }
    }

    public void DisableCrate(GameObject go)
    {
        go.transform.position = Vector3.zero;
        _activeCrate?.Remove(go);
        _inactiveCrate.Add(go);
        go.SetActive(false);
    }

    private IEnumerator CounterToSpawn()
    {
        while (true)
        {
            _timer += Time.deltaTime;
            if (_timer >= (_activeCrate.Count * 2.25f) + 0.25f)
            {
                _timer = 0f;
                SpawnCrate();
            }
            yield return null;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CounterToSpawn());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDisable()
    {
        StopCoroutine(CounterToSpawn());
    }
}
