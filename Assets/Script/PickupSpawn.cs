﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawn : MonoBehaviour
{
    [SerializeField] private List<Transform> _pickupSpawnPos = new List<Transform>();
    [SerializeField] private GameObject _invincibilityPrefab;
    [SerializeField] private GameObject _healPrefab;
    [SerializeField] private GameObject _powerPrefab;
    [SerializeField] private GameObject _randomPrefab;
    [SerializeField] private GameObject _slowPrefab;
    [SerializeField] private GameObject _invertPrefab;

    private bool _canSpawn = true;
   
    private void Update()
    {
        SpawnProcess();
    }

    private void SpawnProcess()
    {
        if (_canSpawn)
        {
            _canSpawn = false;
            StartCoroutine(SpawnTimer());
            SpawnPickup();
        }
    }

    private void SpawnPickup()
    {
        int _rng = Random.Range(0, 6);
        switch (_rng)
        {
            case (0):
                Instantiate(_invincibilityPrefab, _pickupSpawnPos[Random.Range(0, 35)]);
                break;

            case (1):
                Instantiate(_healPrefab, _pickupSpawnPos[Random.Range(0, 35)]);
                break;

            case (2):
                Instantiate(_powerPrefab, _pickupSpawnPos[Random.Range(0, 35)]);
                break;

            case (3):
                Instantiate(_randomPrefab, _pickupSpawnPos[Random.Range(0, 35)]);
                break;

            case (4):
                Instantiate(_slowPrefab, _pickupSpawnPos[Random.Range(0, 35)]);
                break;

            case (5):
                Instantiate(_invertPrefab, _pickupSpawnPos[Random.Range(0, 35)]);
                break;
        }
    }

    private IEnumerator SpawnTimer()
    {
        float timer = 0f;
        float timer2 = Random.Range(5, 17);

        while (timer < timer2)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        _canSpawn = true;
    }
}