using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrateBehaviour : MonoBehaviour
{
    [SerializeField] private Rigidbody _crateRigid = null;
    [SerializeField] private BoxCollider _crateCollider = null;
    [SerializeField] private CrateData _crateData = null;
    [SerializeField] private GameObject _explosionGO = null;
    
    private bool _isHeld;
    private bool _isThrown;
    private bool _isPrimed;

    private Transform _playerHold = null;
    private CrateData.Crate _crateStats;

    private Renderer _crateMaterial = null;
        

    public bool IsThrown()
    {
        return _isThrown;
    }

    private void OnEnable()
    {
        _isThrown = false;
        _isHeld = false;
        _isPrimed = false;
        Debug.Log("false_awake");
        float die = Random.Range(0.00f, 0.99f);
        int crateind = 0;
        if (die >= 0.98f)
        {
            crateind = 3;
        }
        else if(die >= 0.93f)
        {
            crateind = 2;
        }
        else if(die >= 0.68f)
        {
            crateind = 1;
        }
        _crateStats = _crateData.crates[crateind];
        MeshFilter crateMesh = gameObject.GetComponent<MeshFilter>();
        crateMesh.mesh = _crateStats._mesh;
        _crateMaterial = gameObject.GetComponent<Renderer>();
        _crateMaterial.material = _crateStats._material;
    }

    void Start()
    {
    }

    private void LateUpdate()
    {
        if (_isHeld)
        {
            transform.position = _playerHold.position;
            transform.forward = _playerHold.forward;
        }

        if (_isThrown)
        {
            if (_crateRigid.velocity.magnitude < 0.01f)
            {
                _crateRigid.velocity = Vector3.zero;
                _crateRigid.angularVelocity = Vector3.zero;
                _isThrown = false;
            }
        }

    }
    void Update()
    {
        if(_crateRigid.velocity.magnitude < 0.01f && _crateStats._explosive)
        {
            _isPrimed = true;
        }

        if(transform.position.y < -10f)
        {
            CrateManager.Instance.DisableCrate(gameObject);
        }
    }
    public void CratePickUp(Transform playerloc)
    {
        _isHeld = true;
        _isThrown = false;
        _playerHold = playerloc;
        _crateRigid.useGravity = false;
        _crateCollider.enabled = false;
        transform.position = _playerHold.position;
    }
    public void CrateThrown(float throwpwr)
    {
        _crateRigid.useGravity = true;
        _crateCollider.enabled = true;
        _isHeld = false;
        _isThrown = true;
        Vector3 launchVector = new Vector3();
        launchVector = Vector3.Normalize(Vector3.Slerp(_playerHold.forward, Vector3.up, 0.1f));
        _crateRigid.AddForce(launchVector * throwpwr, ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer("Ground") && _isPrimed)
        {
            StartCoroutine(ExplodeCounter(_crateStats._timer));
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (_isThrown)
            {
                if (!_crateStats._explosive)
                {
                    PlayerController player = collision.gameObject?.GetComponent<PlayerController>();
                    if (player != null)
                        player.TakeDamage(_crateStats._damage);
                    CrateManager.Instance.DisableCrate(gameObject);
                }
            }
        }
        
    }

    /*coroutine*/
    private IEnumerator ExplodeCounter(float timer)
    {
        Debug.Log("explode_coroutine_start");
        float elapsed = 0.0f;
        float flasher = 0.0f;
        bool flash = false;

        const float SLOW_FLASHHZ = 0.5f;
        const float FAST_FLASHHZ = 0.05f;
        
        Color origCrateColor = _crateMaterial.material.color;

        do
        {
            flasher += Time.deltaTime;
            elapsed += Time.deltaTime;

            if (elapsed <= 2.0f)
            {
                if (flasher >= SLOW_FLASHHZ)
                {
                    flasher = 0.0f;
                    flash = !flash;
                    if (flash)
                    {
                        _crateMaterial.material.color = Color.yellow;
                    }
                    else
                    {
                        _crateMaterial.material.color = origCrateColor;
                    }
                }
            }
            else
            {
                if (flasher >= FAST_FLASHHZ)
                {
                    flasher = 0.0f;
                    flash = !flash;
                    if (flash)
                    {
                        _crateMaterial.material.color = Color.yellow;
                    }
                    else
                    {
                        _crateMaterial.material.color = origCrateColor;
                    }
                }

            }
            yield return null;
        } while (elapsed < timer);

        GameObject explosion = Instantiate(_explosionGO, gameObject.transform.position, Quaternion.identity);
        explosion.GetComponent<ExplosionBehaviour>().SetExplosion(_crateStats._damage, _crateStats._explosionRadius);
        CrateManager.Instance.DisableCrate(gameObject);
    }
}
