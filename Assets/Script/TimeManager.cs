﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{
    private static TimeManager m_Instance;
    public static TimeManager Instance => m_Instance;

    private bool _canPlay;
    private float _startTimer = 1f;
    private float _timerGame = 120f;

    [SerializeField] private TextMeshProUGUI _three;
    [SerializeField] private TextMeshProUGUI _two;
    [SerializeField] private TextMeshProUGUI _one;
    [SerializeField] private TextMeshProUGUI _go;
    [SerializeField] private TextMeshProUGUI _timer;

    [SerializeField] private Slider _playerOneHP = null;
    [SerializeField] private Slider _playerTwoHP = null;
    [SerializeField] private AudioSource _audio;

    private void Awake()
    {
        if(m_Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        _three.gameObject.SetActive(false);
        _two.gameObject.SetActive(false);
        _one.gameObject.SetActive(false);
        _go.gameObject.SetActive(false);

    }

    private void Update()
    {
        //Debug.Log(_timerGame);
    }

    public void TimerStart()
    {
        StartCoroutine(TimerStartRoutine());
    }

    public bool CanPlayersMove()
    {
        return _canPlay;
    }

    private IEnumerator TimerStartRoutine()
    {
        _audio.Play();
        _three.gameObject.SetActive(true);
        _two.gameObject.SetActive(false);
        _one.gameObject.SetActive(false);
        _go.gameObject.SetActive(false);
        yield return new WaitForSeconds(_startTimer);
        _two.gameObject.SetActive(true);
        _three.gameObject.SetActive(false);
        _one.gameObject.SetActive(false);
        _go.gameObject.SetActive(false);
        yield return new WaitForSeconds(_startTimer);
        _one.gameObject.SetActive(true);
        _three.gameObject.SetActive(false);
        _two.gameObject.SetActive(false);
        _go.gameObject.SetActive(false);
        yield return new WaitForSeconds(_startTimer);
        _go.gameObject.SetActive(true);
        _one.gameObject.SetActive(false);
        _three.gameObject.SetActive(false);
        _two.gameObject.SetActive(false);
        yield return new WaitForSeconds(_startTimer);
        _go.gameObject.SetActive(false);
        _canPlay = true;
        TimerGame();
    }

    private void TimerGame()
    {
        StopCoroutine(TimerStartRoutine());

        if(_canPlay)
        {
            StartCoroutine(TimerGameCoroutine());
        }
    }

    private IEnumerator TimerGameCoroutine()
    {
        while(_timerGame > 0)
        {
            
            _timerGame -= Time.deltaTime;
            _timer.SetText($"{Mathf.Floor(_timerGame / 60)}:{Mathf.Floor((Mathf.Floor(_timerGame) % 60) / 10)}{(Mathf.Floor(_timerGame) % 60) % 10}");
            yield return null;
        }

        if(_timerGame <= 0)
        {
            _timerGame = 0;
            _timer.SetText("GAME FINISHED");
            _canPlay = false;
            if(_playerOneHP.value < _playerTwoHP.value)
            {
                Debug.Log("Player 2 win!");
            }
            else if(_playerOneHP.value > _playerTwoHP.value)
            {
                Debug.Log("Player 1 win!");
            }
            else
            {
                Debug.Log("Tie!");
            }
        }
    }

    public void PlayerDown(int playerindex)
    {
        
        _timerGame = 0;
        _canPlay = false;
        //Debug.Log($"Player {playerindex + 1} win!");
    }


}
