﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickZone : MonoBehaviour
{
    [SerializeField] private Transform _playerLoc = null;
    private List<GameObject> _availableCrate = new List<GameObject>();
    private List<Vector3> _availablePos;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject?.layer == LayerMask.NameToLayer("Crate"))
        {
            CrateBehaviour crate = other.gameObject.GetComponent<CrateBehaviour>();
            if (!crate.IsThrown())
            {
                GameObject go = other.gameObject;
                _availableCrate.Add(go);
            }
        }
        //Debug.Log($"Entry count {_availableCrate.Count}");
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject?.layer == LayerMask.NameToLayer("Crate"))
        {
            CrateBehaviour crate = other.gameObject.GetComponent<CrateBehaviour>();
            if (!crate.IsThrown())
            {
                GameObject go = other.gameObject;
                if (_availableCrate.Contains(go))
                {
                    _availableCrate.Remove(go);
                }
            }
        }
        //Debug.Log($"Exit count {_availableCrate.Count}");
    }
    
    public GameObject PickUpClosest()
    {
        if (_availableCrate.Count > 0)
        {
            GameObject gotogo = null;
            foreach (GameObject go in _availableCrate)
            {
                float dist = 2.0f;
                if (Mathf.Abs(Vector3.Magnitude(go.transform.position - _playerLoc.position)) < dist)
                {
                    gotogo = go;
                }
            }
            _availableCrate.Remove(gotogo);

            return gotogo;
        }
        else
        {
            return null;
        }
    }
}
