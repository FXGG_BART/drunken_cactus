﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionBehaviour : MonoBehaviour
{
    [SerializeField] private Rigidbody _rigidExplode = null;
    [SerializeField] private SphereCollider _collider = null;

    private int _damage = 0;
    private float _radius = 0;
    private int _frameAlive = 5;

    public void SetExplosion (int dmg, float radi)
    {
        _damage = dmg;
        _radius = radi;
        _collider.radius = _radius;
    }

    private void Update()
    {
        _frameAlive--;
        if (_frameAlive == 0)
            Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("Player") )
        {
            PlayerController player = other.gameObject?.GetComponent<PlayerController>();
            if (player != null)
                player.TakeDamage(_damage);
        }
        if(other.gameObject.layer == LayerMask.NameToLayer("Crate"))
        {
            Rigidbody crate = other.gameObject?.GetComponent<Rigidbody>();
            if (crate != null)
            crate.AddForce(Vector3.Normalize(other.transform.position - transform.position), ForceMode.Impulse);
        }
    }
}
