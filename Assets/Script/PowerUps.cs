﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour
{
    [SerializeField] private PowerUpData _data;
    [SerializeField] private int index;

    [SerializeField] private float _rotationSpeed = 50f;

    private int _rng = 0;


    private void Update()
    {
        gameObject.transform.Rotate(new Vector3(0, 1, 0) * _rotationSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            PlayerController script = other.gameObject.GetComponent<PlayerController>();

            switch (index)
            {
                case (0):
                    script.PowerupPickup(0);
                    break;
                case (1):
                    script.PowerupPickup(1);
                    break;
                case (2):
                    script.PowerupPickup(2);
                    break;
                case (3):
                    script.PowerupPickup(3);
                    break;
                case (4):
                    break;
                case (5):
                    _rng = Random.Range(0, 5);
                    script.PowerupPickup(_rng);
                    break;
               
            }
            Destroy(gameObject);
        }
    }

}
